<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PiedWeb\FacebookScraper\FacebookScraper;
use TwitterNoAuth\Twitter;

class TwitterController extends Controller
{
    public function test(){

        return view('twitter.index');
    }

    public function index1(){

        return view('twitter.index1');
    }

    public function store(Request $request){
        $data = $request->all();
        $tweet_id=$data['tweet'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://twitter60.p.rapidapi.com/user_info?user_name=".$tweet_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: twitter60.p.rapidapi.com",
                "x-rapidapi-key: 9d50e418famshcd00280aef6c02cp1b4406jsn9b899ecfb327"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
           echo "cURL Error #:" . $err;
        } else {
            $json = json_decode($response);
            $data = $json->data->user->result->legacy;
            return view('twitter.index', compact('data'));
        }

    }

    public function userInfoView(){

        return view('twitter.user_info');
    }

    public function userInfoStore(Request $request){

        $data = $request->all();
        $user_name=$data['user_name'];


        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://twitter60.p.rapidapi.com/tweet_detail?tweet_id=".$user_name,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: twitter60.p.rapidapi.com",
                "x-rapidapi-key: 9d50e418famshcd00280aef6c02cp1b4406jsn9b899ecfb327"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);



        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $json = json_decode($response);

            $data = $json->data->threaded_conversation_with_injections->instructions[0]->entries;

            return view('twitter.user_info', compact('data'));
        }

    }


}
