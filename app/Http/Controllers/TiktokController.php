<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TiktokController extends Controller
{
    public function tiktok(){
        return view('tiktok.tiktok_name');

    }
    public function new_store(Request $request){
        $api=new \Sovit\TikTok\Api(array(/* config array*/));
        $user_name=$request->name;
        $hashtag=$request->hashtag;
        $userFeed=$api->getUserFeed($user_name,$maxCursor=0); // Use This More Detail
         $user=$userFeed;
        //get Hashtag
        $challengeFeed='';
        if( $request->input('hashtag')){
            $challengeFeed=$api->getChallengeFeed($hashtag,$maxCursor=0);
        }

        return view('tiktok.index',compact('user','challengeFeed'));

    }
}
