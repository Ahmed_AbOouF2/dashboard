<?php

namespace App\Http\Controllers;
//use Faryar76\TlgScrapper;
use App\Http\Packages\src\TlgScrapper;
use Illuminate\Http\Request;

class TelegramController extends Controller
{
    public function index(){

        return view('telegram.index');
    }
    public function telegrams(Request $request){
        $name = $request->name;
//        $tCrawl = new TCrawler();
//
//        $tCrawl->setProxy("http://username:password@host:port");
//        $s =   $tCrawl->setChannel("KamillionBets");
//
//        $formWithTemplate = false;
//        $lastMessgae    = $tCrawl->crawler()->getLastId();
//


        $tlg=new TlgScrapper();
       $s =  $tlg->load($name);
        $messages = $s->getMessages();
        $messages = $messages->getArrayCopy();

        $channel_name =  $s->getName();

        return view('telegram.telegram_name',compact('messages','channel_name'));
    }
}
