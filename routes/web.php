<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


////////////////////// Instgram //////////////////////
Route::get('new-insta','InstgramController@new_index')->name('insta.new');
Route::post('new-instas','InstgramController@new_store')->name('insta.new.store');
////////////////////// Instgram //////////////////////



////////////////////// Tiktok //////////////////////
Route::get('tiktok','TiktokController@tiktok')->name('tiktok');
Route::post('new-tiktok','TiktokController@new_store')->name('tiktok.new.store');
////////////////////// Tiktok //////////////////////



////////////////////// telegram //////////////////////
 Route::get('telegram','TelegramController@index')->name('telegram');
 Route::post('telegrams','TelegramController@telegrams')->name('telegrams');
////////////////////// telegram //////////////////////



////////////////////// youtube //////////////////////
Route::prefix('youtube')->group(function () {
    Route::get('/', 'YoutubeController@index')->name('youtube.index1');
    Route::get('/data', 'YoutubeController@index1')->name('youtube.index');
    Route::get('/results', 'YoutubeController@results')->name('youtube.results');
    Route::get('/watch/{id}', 'YoutubeController@watch')->name('youtube.watch');
    Route::get('/channel', 'YoutubeController@channel')->name('youtube.channel');
    Route::get('/channel-subscribers', 'YoutubeController@subscribers')->name('youtube.subscribers');
    Route::get('/video-data', 'YoutubeController@videoData')->name('youtube.videoData');
    Route::post('/get-video-data', 'YoutubeController@getYoutubeVideoID')->name('youtube.getYoutubeVideoID');
    Route::get('/get-channel', 'YoutubeController@getChannels')->name('youtube.getChannels');
    Route::get('/get-channel-by-name', 'YoutubeController@getChannelByName');
    Route::get('/getChannelById', 'YoutubeController@alaouy')->name('youtube.package');
    Route::post('/alaouy-package', 'YoutubeController@getChannelById')->name('youtube.alaouySubmit');
});
////////////////////// youtube //////////////////////


Route::get('/fb', 'TwitterController@test')->name('fb');
Route::get('/fb/userInfo', 'TwitterController@userInfoView')->name('fb.userInfo');
Route::get('/index1', 'TwitterController@index1')->name('twitter.index1');

Route::get('/fb/store', 'TwitterController@store')->name('fb.store');

Route::get('/fb/userInfo/store', 'TwitterController@userInfoStore')->name('fb.userInfo.store');


Route::get('/{page}', 'AdminController@index');




