@extends('layouts.master')
@section('css')
@endsection
@section('page-header')
    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto">Pages</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ Empty</span>
            </div>
        </div>

    </div>
    <!-- breadcrumb -->
@endsection
@section('content')
    <!-- row -->
    <div class="row row-sm">

        <div class="col-lg-8">

            <div class="card">
                <div class="card-body">
                    <h2>Channel Name : {{$channel_name}}</h2>
{{--                    <h2>Description : {{$decription}}</h2>--}}
                    <table class="table table-bordered">
                        @foreach($messages as $message)
                            <tr>
                                <td>{{$message['id']}}</td>
                                <td>{{$message['date']['date']}}</td>
                                <td>{{$message['views']}}</td>
                                <td>{{$message['text']}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- row closed -->
    </div>
    <!-- Container closed -->
    </div>
    <!-- main-content closed -->



@endsection
@section('js')
@endsection
