@extends('layouts.master')
@section('css')
@section('content')
    <form action="{{ route('fb.store') }}" method="get" class="mb-5 mt-5">
        <div class="row">
            <div class="col-sm-8 form-group">
                <input type="text" class="form-control" name="tweet" placeholder="user info........" >
            </div>
            <div class="col-sm-4">
                <input type="submit" value="Search" class="btn btn-success">
            </div>
        </div>
    </form>

    <div class="row">
        @if(!empty($data))
                <table class="table table-bordered">
                    <tr>
                        <td>Name</td>
                        <td>{{$data->name}}</td>
                    </tr>
                    <tr>
                        <td>Protected</td>
                        <td>{{$data->protected}}</td>
                    </tr>
                    <tr>
                        <td>Created At</td>
                        <td>{{$data->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Followers Count</td>
                        <td>{{$data->followers_count}}</td>
                    </tr>
                    <tr>
                        <td>Favorite Count</td>
                        <td>{{$data->favourites_count}}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{{$data->description}}</td>
                    </tr>
                    <tr>
                        <td>Friends Count</td>
                        <td>{{$data->friends_count}}</td>
                    </tr>
                    <tr>
                        <td>Location</td>
                        <td>{{$data->location}}</td>
                    </tr>
                    <tr>
                        <td>Media Count</td>
                        <td>{{$data->media_count}}</td>
                    </tr>
                    <tr>
                        <td>Profile Banner</td>
                        <td><img src="{{$data->profile_banner_url ?? ''}}" width="300px" height="300px"></td>
                    </tr>
                    <tr>
                        <td>Profile Image</td>
                        <td><img src="{{$data->profile_image_url_https ?? ''}}" width="300px" height="300px"></td>
                    </tr>
                    <tr>
                        <td>Screen Name</td>
                        <td>{{$data->screen_name}}</td>
                    </tr>
                    <tr>
                        <td>Statuses Count</td>
                        <td>{{$data->statuses_count}}</td>
                    </tr>


                </table>
        @endif

    </div>
@stop
