@extends('layouts.master')
@section('css')
@section('content')
    <form action="{{ route('fb.userInfo.store') }}" method="get" class="mb-5 mt-5">
        <div class="row">
            <div class="col-sm-8 form-group">
                <input type="text" class="form-control" name="user_name" placeholder="tweet detail........" >
            </div>
            <div class="col-sm-4">
                <input type="submit" value="Search" class="btn btn-success">
            </div>
        </div>
    </form>

    <div class="row">
        @if(!empty($data))
            <?php $legacy = $data[0]->content->itemContent->tweet_results->result->legacy ; ?>
            <table class="table table-bordered">
                <tr>
                    <td>Created_at</td>
                    <td>{{$legacy->created_at}}</td>
                </tr>

                <tr>
                    <td>favorite_count</td>
                    <td>{{$legacy->favorite_count}}</td>
                </tr>
                <tr>
                    <td>tweet text</td>
                    <td>{{$legacy->full_text}}</td>
                </tr>
{{--                <tr>--}}
{{--                    <td>mentions</td>--}}
{{--                    <td> {{ $legacy->entities->user_mentions[0]->name }}</td>--}}
{{--                </tr>--}}
                <tr>
                    <td>reply_count</td>
                    <td>{{ $legacy->reply_count }} </td>
                </tr>
                <tr>
                    <td>retweet_count</td>
                    <td>{{ $legacy->retweet_count }}</td>
                </tr>
                <tr>
                    <td>URL Attached</td>
                    <td>{{$data[0]->content->itemContent->tweet_results->result->rest_id}}</td>
                </tr>
            </table>
        @endif
    </div>
@stop
