@extends('layouts.master')
@section('css')
@endsection
@section('page-header')
    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto">Pages</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ Empty</span>
            </div>
        </div>

    </div>
    <!-- breadcrumb -->
@endsection
@section('content')
    <!-- row -->
    <div class="row row-sm">
        <div class="col-lg-4">
            <div class="card mg-b-20">
                <div class="card-body">
                    <div class="pl-0">
                        <div class="main-profile-overview">
                            <div class="main-img-user profile-user">

                                <img alt="" src="{{$user->info->detail->user->avatarMedium}}"><a class="fas fa-camera profile-edit" href="JavaScript:void(0);"></a>
                            </div>
                            <div class="d-flex justify-content-between mg-b-20">
                                <div>
                                    <h5 class="main-profile-name">{{$user->info->detail->user->nickname}}</h5>
                                    <p class="main-profile-name-text"> @ {{$user->info->detail->user->uniqueId}}</p>
                                    <p class="main-profile-name-text">Created at : {{ Carbon\Carbon::createFromTimestamp($user->info->detail->user->createTime, 'Europe/London')->format('Y-m-d ')  }}</p>
                                </div>
                            </div>
                            <h6>Bio</h6>
                            <div class="main-profile-bio">
                                {{$user->info->detail->user->signature}}
                            </div><!-- main-profile-bio -->
                            <div class="row">
                                <div class="col-md-3 col mb20">
                                    @php
                                        if ($user->info->detail->stats->followerCount < 900) {
                                                $n_format = number_format($user->info->detail->stats->followerCount);
                                            }
                                            elseif($user->info->detail->stats->followerCount < 900000  ){
                                                     $n_format = number_format($user->info->detail->stats->followerCount/1000,1 ).' k';
                                            }elseif ($user->info->detail->stats->followerCount <900000000 ){
                                                         $n_format = number_format($user->info->detail->stats->followerCount/1000000,1 ).' m';

                                            }
                                    @endphp
                                    <h5> {{$n_format}}</h5>
                                    <h6 class="text-small text-muted mb-0">Followers</h6>
                                </div>
                                <div class="col-md-3 col mb20">
                                    <h5>{{$user->info->detail->stats->followingCount}}</h5>
                                    <h6 class="text-small text-muted mb-0">Following</h6>
                                </div>
                                <div class="col-md-3 col mb20">
                                    <h5>{{$user->info->detail->stats->videoCount}}</h5>
                                    <h6 class="text-small text-muted mb-0">Videos</h6>
                                </div>
                                <div class="col-md-3 col mb20">
                                    @php
                                        if ($user->info->detail->stats->heartCount < 900) {
                                                $n_format = number_format($user->info->detail->stats->heartCount);
                                            }
                                            elseif($user->info->detail->stats->heartCount < 900000  ){
                                                     $n_format = number_format($user->info->detail->stats->heartCount/1000,1 ).' k';
                                            }elseif ($user->info->detail->stats->heartCount <900000000 ){
                                                         $n_format = number_format($user->info->detail->stats->heartCount/1000000,1 ).' m';

                                            }
                                    @endphp
                                    <h5> {{$n_format}}</h5>

                                    <h6 class="text-small text-muted mb-0">Heart</h6>
                                </div>
                            </div>



                        </div><!-- main-profile-overview -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">

            <div class="card">
                <div class="card-body">
                    <div class="tabs-menu ">
                        <!-- Tabs -->
                        <ul class="nav nav-tabs profile navtab-custom panel-tabs">
                            <li class="active">
                                <a href="#home" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="las la-user-circle tx-16 mr-1"></i></span> <span class="hidden-xs">Profile Videos</span> </a>
                            </li>

                            <li class="">
                                <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="las la-cog tx-16 mr-1"></i></span> <span class="hidden-xs">Search Hashtags</span> </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content border-left border-bottom border-right border-top-0 p-4">
                        <div class="tab-pane active" id="home">
                            <div class="row row-sm">
                                @if(!empty($user->items))

                                        <div class="row">
                                            @foreach($user->items as $item)
                                                <div class="col-sm-4">
                                                    <div class="border p-1 card thumb">

                                                        <a href="" target="_blank" class="image-hashpup" title="Screenshot-2">
                                                            <iframe width="270" height="290" src="{{$item->video->playAddr}}" frameborder="0" ></iframe>
                                                        </a>
                                                        <h4 class="text-center tx-14 mt-3 mb-0">{{$item->desc}}</h4>
                                                        <div class="ga-border"></div>
                                                        <p class="text-muted text-center"><small>{{ Carbon\Carbon::createFromTimestamp($item->createTime, 'Europe/London')->format('Y-m-d h:i a')  }}</small> <br> <small> <b>Comments : </b> {{$item->stats->commentCount}}</small> <br> <small> <b>Likes :</b> {{$item->stats->diggCount}}</small>

                                                            <br> <small> <b>Play : </b> {{$item->stats->playCount}}</small>  <br> <small> <b>Share : </b> {{$item->stats->shareCount}}</small>
                                                        </p>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>

                                @else
                                    <p>There is No Videos</p>
                                @endif
                            </div>
                        </div>

                        <div class="tab-pane" id="settings">
                            <div class="row">
                                @if(!empty($challengeFeed))
                                    @foreach($challengeFeed->items as $item)
                                        <div class="col-sm-4">
                                            <div class="border p-1 card thumb">

                                                <a href="" target="_blank" class="image-hashpup" title="Screenshot-2">

                                                    <iframe width="200" height="290" src="{{$item->video->playAddr}}" frameborder="0" ></iframe>
                                                </a>
                                                <h4 class="text-center tx-14 mt-3 mb-0">{{$item->desc}}</h4>
                                                <div class="ga-border"></div>
                                                <p class="text-muted text-center"><small>{{ Carbon\Carbon::createFromTimestamp($item->createTime, 'Europe/London')->format('Y-m-d h:i a')  }}</small> <br> <small> <b>Comments : </b> {{$item->stats->commentCount}}</small> <br> <small> <b>Likes :</b> {{$item->stats->diggCount}}</small>

                                                    <br> <small> <b>Play : </b> {{$item->stats->playCount}}</small>  <br> <small> <b>Share : </b> {{$item->stats->shareCount}}</small>
                                                </p>
                                            </div>
                                        </div>


                                    @endforeach
                                @else
                                    <p>There is No Hashtag</p>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row closed -->
    </div>
    <!-- Container closed -->
    </div>
    <!-- main-content closed -->



@endsection
@section('js')
@endsection
