@extends('layouts.master')
@section('css')
@endsection
@section('page-header')
				<!-- breadcrumb -->
				<div class="breadcrumb-header justify-content-between">
					<div class="my-auto">
						<div class="d-flex">
							<h4 class="content-title mb-0 my-auto">Pages</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ Empty</span>
						</div>
					</div>

				</div>
				<!-- breadcrumb -->
@endsection
@section('content')
    <!-- row -->
    <div class="row row-sm">
        <div class="col-lg-4">
            <div class="card mg-b-20">
                <div class="card-body">
                    <div class="pl-0">
                        <div class="main-profile-overview">
                            <div class="main-img-user profile-user">
                                @php
                                  $image=  base64_encode(file_get_contents($profile->getProfilePicture()))
                                @endphp
                                <img alt="" src="data:image/x-icon;base64,<?= $image ?>"><a class="fas fa-camera profile-edit" href="JavaScript:void(0);"></a>
                            </div>
                            <div class="d-flex justify-content-between mg-b-20">
                                <div>
                                    <h5 class="main-profile-name">{{$profile->getFullName()}}</h5>
                                    <p class="main-profile-name-text">{{$profile->getUserName()}}</p>
                                </div>
                            </div>
                            <h6>Bio</h6>
                            <div class="main-profile-bio">
                                {{$profile->getBiography()}}
                            </div><!-- main-profile-bio -->
                            <div class="row">
                                <div class="col-md-4 col mb20">
                                    @php
                                        if ($profile->getFollowers() < 900) {
                                                $n_format = number_format($profile->getFollowers());
                                            }
                                            elseif($profile->getFollowers() < 900000  ){
                                                     $n_format = number_format($profile->getFollowers()/1000,1 ).'k';
                                            }elseif ($profile->getFollowers() <900000000 ){
                                                         $n_format = number_format($profile->getFollowers()/1000000,1 ).'m';

                                            }
                                    @endphp
                                    <h5> {{$n_format}}</h5>
                                    <h6 class="text-small text-muted mb-0">Followers</h6>
                                </div>
                                <div class="col-md-4 col mb20">
                                    <h5>{{$profile->getFollowing()}}</h5>
                                    <h6 class="text-small text-muted mb-0">Following</h6>
                                </div>
                                <div class="col-md-4 col mb20">
                                    <h5>{{$profile->getMediaCount()}}</h5>
                                    <h6 class="text-small text-muted mb-0">Posts</h6>
                                </div>
                            </div>
                            <hr class="mg-y-30">
                            <label class="main-content-label tx-13 mg-b-20">Social</label>
                            <div class="main-profile-social-list">
                                    <div class="media">
                                        <div class="media-icon bg-primary-transparent text-primary">
                                            <i class="icon ion-logo-github"></i>
                                        </div>
                                        @if($profile->getExternalUrl())

                                        <div class="media-body">
                                            <span>Links</span> <a href="">{{$profile->getExternalUrl()}}</a>
                                        </div>
                                        @endif

                                    </div>


                            </div>


                        </div><!-- main-profile-overview -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">

            <div class="card">
                <div class="card-body">
                    <div class="tabs-menu ">
                        <!-- Tabs -->
                        <ul class="nav nav-tabs profile navtab-custom panel-tabs">
                            <li class="active">
                                <a href="#home" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="las la-user-circle tx-16 mr-1"></i></span> <span class="hidden-xs">Profile Stories</span> </a>
                            </li>
                            <li class="">
                                <a href="#profile" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="las la-images tx-15 mr-1"></i></span> <span class="hidden-xs">Profile Posts</span> </a>
                            </li>
                            <li class="">
                                <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="las la-cog tx-16 mr-1"></i></span> <span class="hidden-xs">Search Hashtags</span> </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content border-left border-bottom border-right border-top-0 p-4">
                        <div class="tab-pane" id="home">
                            <div class="row row-sm">
                                @if(!empty($stories))
                                @foreach($stories as $story)
                                    <div class="col-sm-12 col-xl-4 col-lg-12 col-md-12">
                                        <div class="card ">
                                            <div class="card-body">
                                                <div class="counter-status d-flex md-mb-0">
                                                    <div class="main-img-user profile-user">
                                                        @php
                                                            $image=  base64_encode(file_get_contents($story->getDisplayUrl()));
                                                        @endphp
                                                        <img alt="" src="data:image/x-icon;base64,<?= $image ?>" style="width: 100px !imhashrtant;height: 46px !imhashrtant;">
                                                    </div>
                                                    <div class="mr-auto">
                                                        <p class="tx-13 text-center"> Type : @if($story->getVideoResources()) Video - Duration : {{$story->getVideoDuration()}} @else Image  @endif </p>
                                                        <p class="text-muted mb-0 tx-11">Add Date :{{\Carbon\Carbon::parse($story->getTakenAtDate())->diffForHumans()}} </p>
                                                        <p class="text-muted mb-0 tx-11">Expired Date :{{$story->getExpiringAtDate()->format('Y-m-d')}}</p>
                                                        @if($story->getHashtags())
                                                            <p class="text-muted mb-0 tx-11"> #:
                                                                @foreach($story->getHashtags() as $hash)
                                                                    <span>{{$hash}}</span> -
                                                                @endforeach
                                                            </p>
                                                        @endif
                                                        @if($story->getMentions())
                                                            <p class="text-muted mb-0 tx-11"> @:
                                                                @foreach($story->getMentions() as $men)
                                                                    <span>{{$men}}</span> -
                                                                @endforeach
                                                            </p>
                                                        @endif
                                                        @if($story->getLocations())
                                                            <p class="text-muted mb-0 tx-11">Loc :
                                                                @foreach($story->getLocations() as $loc)
                                                                    <span>{{$loc}}</span> -
                                                                @endforeach
                                                            </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @else
                                <p>There is No Stories</p>
                                    @endif
                            </div>
                        </div>
                        <div class="tab-pane active" id="profile">
                            <div class="row">
                                @foreach($profile->getMedias() as $hash)
                                <div class="col-sm-4">
                                    <div class="border p-1 card thumb">
                                        @php
                                            $image=  base64_encode(file_get_contents($hash->getDisplaySrc()))
                                        @endphp
                                        <a href="{{$hash->getLink()}}" target="_blank" class="image-hashpup" title="Screenshot-2"> <img src="data:image/x-icon;base64,<?= $image ?>" class="thumb-img" alt="work-thumbnail" width="200" height="290"> </a>
                                        <h4 class="text-center tx-14 mt-3 mb-0">{{$hash->getCaption()}}</h4>
                                        <div class="ga-border"></div>
                                        <p class="text-muted text-center"><small>{{\Carbon\Carbon::parse($hash->getDate())->diffForHumans()}}</small> <br> <small> <b>Comments : </b> {{$hash->getComments()}}</small> <br> <small> <b>Likes :</b> {{$hash->getLikes()}}</small>

                                             <br>   @if($hash->getHashtags()) HashTags:  @foreach($hash->getHashtags() as $hash) <small>  {{$hash}}</small>  @endforeach   @endif
                                        </p>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="tab-pane" id="settings">
                            <div class="row">
                                @if(!empty($medias))
                                @foreach($medias as $hash)
                                    <div class="col-sm-4">
                                        <div class="border p-1 card thumb">
                                            @php
                                                $image=  base64_encode(file_get_contents($hash->getDisplaySrc()))
                                            @endphp
                                            <a href="{{$hash->getLink()}}" target="_blank" class="image-hashpup" title="Screenshot-2"> <img src="data:image/x-icon;base64,<?= $image ?>" class="thumb-img" alt="work-thumbnail" width="200" height="290"> </a>
                                            <h4 class="text-center tx-14 mt-3 mb-0">{{$hash->getCaption()}}</h4>
                                            <div class="ga-border"></div>
                                            <p class="text-muted text-center"><small>{{\Carbon\Carbon::parse($hash->getDate())->diffForHumans()}}</small> <br> <small> <b>Comments : </b> {{$hash->getComments()}}</small> <br> <small> <b>Likes :</b> {{$hash->getLikes()}}</small>

                                                <br> <small>Type :  {{$hash->getTypeName()}}</small>
                                            </p>
                                        </div>
                                    </div>


                                @endforeach
                                @else
                                    <p>There is No Hashtag</p>
                                    @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row closed -->
    </div>
    <!-- Container closed -->
    </div>
    <!-- main-content closed -->



@endsection
@section('js')
@endsection
