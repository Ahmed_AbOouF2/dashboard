@extends('layouts.master')
@section('css')
@endsection
@section('page-header')
				<!-- breadcrumb -->
                                <div class="breadcrumb-header justify-content-between">
                                    <div class="my-auto">
                                        <div class="d-flex">
                                            <h4 class="content-title mb-0 my-auto">Pages</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ Empty</span>
                                        </div>
                                    </div>

				</div>
				<!-- breadcrumb -->
@endsection
@section('content')
				<!-- row -->
				<div class="row">
                    <div class="col-4">
                        <form role="form" action="{{route('insta.new.store')}}" method="POST" >
                            @csrf
                            <div class="form-group">
                                <label for="FullName">Name</label>
                                <input type="text"  id="FullName" name="name" class="form-control">
                                @error('name')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="Hashtag">Search Hashtag</label>
                                <input type="text"  id="Hashtag" name="hashtag" class="form-control">
                            </div>

                            <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Save</button>
                        </form>

                    </div>
				</div>
				<!-- row closed -->
			</div>
			<!-- Container closed -->
		</div>
		<!-- main-content closed -->
@endsection
@section('js')
@endsection
